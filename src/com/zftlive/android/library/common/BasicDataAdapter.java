package com.zftlive.android.library.common;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.zftlive.android.library.base.BaseMAdapter;
import com.zftlive.android.library.base.BaseViewHolder;

/**
 * 基本的列表数据展示Adapter，只需要传入BaseViewHolder的实现类即可
 * 具体示例写法见ImageListviewActivity.java
 * 
 * @see http://git.oschina.net/zftlive/AjavaAndroidSample/blob/master/src/com/zftlive/android/sample/image/ImageListviewActivity.java
 * @author 曾繁添
 * @version 1.0
 *
 */
public class BasicDataAdapter<T> extends BaseMAdapter {

	/**
	 * Item控件缓存ViewHolder
	 */
	private BaseViewHolder mViewHolder;
	
	/**
	 * 上下文
	 */
	private Context mContext;
	
	public BasicDataAdapter(BaseViewHolder mViewHolder){
		this.mViewHolder = mViewHolder;
		this.mContext = mViewHolder.getContext();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if(null == convertView){
			try {
				mViewHolder = mViewHolder.getClass().newInstance();
				mViewHolder.setContext(mContext);
			} catch (Exception e) {
				Log.e(TAG, "newInstance error-->"+e.getMessage());
			}
			convertView = mViewHolder.inflateLayout();;
			mViewHolder.initView(convertView);
			convertView.setTag(mViewHolder);
		}else{
			mViewHolder = (BaseViewHolder)convertView.getTag();
		}
		//设置数据
		T rowData = (T)getItem(position);
		mViewHolder.fillData(rowData, position);
		return convertView;
	}
}